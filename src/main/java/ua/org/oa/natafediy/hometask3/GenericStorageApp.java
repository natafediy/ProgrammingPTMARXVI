package ua.org.oa.natafediy.hometask3;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Nata Fediy
 * Date: 5/4/2016
 * Time: 3:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class GenericStorageApp {
    public static void main(String[] args) {
        int size = 15;
        GenericStorage<String> genericStorage = new GenericStorage<String>();
        GenericStorage<String> genericStorageWithSize = new GenericStorage<String>(size);

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Add elements to dynamic array with a default size (genericStorage): ");
        genericStorage.add("111 222 888 4567");
        genericStorage.add("asdAAA SD33SD");
        genericStorage.add("j4");
        genericStorage.add("Test");
        genericStorage.add("111 222 888 4567");
        genericStorage.add("asdAAA SD33SD");
        genericStorage.add("j4");
        genericStorage.add("Test");
        genericStorage.add("111 222 888 4567");
        genericStorage.add("asdAAA SD33SD");
        genericStorage.add("j4");
        genericStorage.add("Test");
        System.out.println("\nArray with added elements: " + Arrays.toString(genericStorage.getAll()));
        System.out.println("This dynamic array has the following size : " + genericStorage.getFullArraySize());
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Add elements to array with required size (genericStorageWithSize): ");
        for (int i = 0; i < size; i++) {
            genericStorageWithSize.add("9");
        }
        System.out.println("\ngenericStorageWithSize Array: " + Arrays.toString(genericStorageWithSize.getAll()));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("genericStorage Array: " + Arrays.toString(genericStorage.getAll()));
        System.out.println("Get element by id: ");
        System.out.println("id =   0 : " + genericStorage.get(0));
        System.out.println("id =   1 : " + genericStorage.get(1));
        System.out.println("id =   11 : " + genericStorage.get(11));

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Get all array: " + Arrays.toString(genericStorage.getAll()));

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Update element by id: \nid = 2, value = \"UPDATE\"");
        System.out.println("Before update : " + Arrays.toString(genericStorage.getAll()));
        System.out.println("After update  : " + Arrays.toString(genericStorage.update(2, "UPDATE")));

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Delete element by id: ");
        System.out.println("Before deleting element by id =  0 : " + Arrays.toString(genericStorage.getAll()));
        System.out.println("id =   0 : " + genericStorage.delete(0));
        System.out.println("After deleting element by id =   0 : " + Arrays.toString(genericStorage.getAll()));
        System.out.println("Before deleting element by id =  2 : " + Arrays.toString(genericStorage.getAll()));
        System.out.println("id =   2 : " + genericStorage.delete(2));
        System.out.println("After deleting element by id =   2 : " + Arrays.toString(genericStorage.getAll()));

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Delete element by value: " + genericStorage.delete("UPDATE"));
        System.out.println("Array after deleting element by value : " + Arrays.toString(genericStorage.getAll()));

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Size of the filled in array (genericStorage): " + genericStorage.getFullArraySize());
        System.out.println("Size of the filled in array (genericStorageWithSize): " + genericStorageWithSize.getFullArraySize());

    }
}
