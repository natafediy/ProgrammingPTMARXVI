package ua.org.oa.natafediy.hometask1.part2;

/**
 * Created with IntelliJ IDEA.
 * User: Nata Fediy
 * Date: 4/13/2016
 * Time: 9:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class Car {

    private String carType = "vehicle";

    private static String carType1 = "truck";
    private static String carType2 = "bus";

    public String getCarType() {
        return carType;
    }

    public static void main(String[] args) {
        Car car = new Car(carType1);
        Car car1 = new Car(carType1) {
            String carType1 = "vehicle/truk";

            public String toString() {

                return "Car{" +
                        "car1= " + carType1 +
                        '}';
            }

            @Override
            public boolean equals(Object o) {
                return super.equals(o);
            }
        };


        Car car2 = new Car(carType2) {
            String carType2 = "vehicle/bus";
            @Override
            public String toString() {

                return "Car{" +
                        "car2= " + carType2 +
                        '}';
            }

            @Override
            public boolean equals(Object o) {
                return super.equals(o);
            }

        };

        System.out.println(car.toString());
        System.out.println(car1.toString());
        System.out.println(car2.toString());
        System.out.println(car.equals(car1));

    }

    public Car(String carType) {
        this.carType = carType;
    }


    @Override
    public String toString() {
        return "Car{" +
                "carType='" + carType + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        return getCarType().equals(car.getCarType());

    }

    @Override
    public int hashCode() {
        return getCarType().hashCode();
    }
}
