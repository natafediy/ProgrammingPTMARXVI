package ua.org.oa.natafediy.hometask2.part1;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Nata Fediy
 * Date: 4/18/2016
 * Time: 9:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class StringUtilsApp {
    public static void main(String[] args) throws IOException {
        String phrase = "Hello Friends!";
        String phrase1 = "abc";
        String phrase2 = "aa abb c ccc";
        String phrase3 = "racebcar";
        String palindrome = "race car";
        String palindrome2 = "Was it a car or a cat I saw?";
        String nextPhrase = "Inserts the string into this character sequence";
        String nextPhrase1 = "Hello Friends";
        String nextPhrase0 = "Hello";
        String listOfPhrases = "Race. Hello Friends. Inserts the string into this character sequence.";
        String regex = "[abc]+";
        String date1 = "23.04.1579";
        String date2 = "04.23.1579";
        String date3 = "02.31.2000";
        String date4 = "09.31.2000";
        String emailExample1 = "very.common@example.com";
        String emailExample2 = "Abc.example.com";
        String emailExample3 = "A@b@c@example.com";
        String emailExample4 = "user-123@example.org";
        String emailExample5 = "user158@example.com";
        String[] arrayOfNumbers = {"(+351) 282 43 50 50", "+9(019)191-99-08", "555-8909",
                "001 6867684", "001 6867684x1", "+1(234)567-89-01", "1-234-567-8901", "1-234-567-8901",
                "1(23)567-89-01", "+1(234)5678901", "(123)8575973", "(0055)(123)8575973", "+3(512)824-35-50"};


        System.out.println("\nSub-task 1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(phrase + "\n" + StringUtils.reverseString(phrase));
        System.out.println("\nSub-task 2 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Is '" + palindrome + "' phrase a palindrome? " + StringUtils.isPhrasePalindrome(palindrome));
        System.out.println("Is '" + phrase + "' phrase a palindrome? " + StringUtils.isPhrasePalindrome(phrase));
        System.out.println("Is '" + palindrome2 + "' phrase a palindrome? " + StringUtils.isPhrasePalindrome(palindrome2));
        System.out.println("\nSub-task 3 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(StringUtils.setNewSymbols(palindrome));
        System.out.println(StringUtils.setNewSymbols(phrase));
        System.out.println(StringUtils.setNewSymbols(palindrome2));
        System.out.println("\nSub-task 4 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(nextPhrase + "\n" + StringUtils.changeFirstToLastWord(nextPhrase));
        System.out.println(nextPhrase1 + "\n" + StringUtils.changeFirstToLastWord(nextPhrase1));
        System.out.println(nextPhrase0 + "\n" + StringUtils.changeFirstToLastWord(nextPhrase0));
        System.out.println("\nSub-task 5 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(listOfPhrases + "\n" + StringUtils.changeFirstToLastWordEverySentence(listOfPhrases));
        System.out.println("\nSub-task 6 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Does " + phrase + " contain '" + regex + "' ?  " + StringUtils.checkLineContainsSelectedSymbols(phrase, regex));
        System.out.println("Does " + phrase1 + " contain '" + regex + "' ?  " + StringUtils.checkLineContainsSelectedSymbols(phrase1, regex));
        System.out.println("Does " + phrase2 + " contain '" + regex + "' ?  " + StringUtils.checkLineContainsSelectedSymbols(phrase2, regex));
        System.out.println("Does " + phrase3 + " contain '" + regex + "' ?  " + StringUtils.checkLineContainsSelectedSymbols(phrase3, regex));
        System.out.println("\nSub-task 7 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Is " + date1 + " a date with MM.DD.YYYY format? " + StringUtils.checkTextIsDateSelectedFormat(date1));
        System.out.println("Is " + date1 + " a date with MM.DD.YYYY format? " + StringUtils.checkTextIsDateSelectedFormatV2(date1));
        System.out.println("Is " + date2 + " a date with MM.DD.YYYY format? " + StringUtils.checkTextIsDateSelectedFormat(date2));
        System.out.println("Is " + date2 + " a date with MM.DD.YYYY format? " + StringUtils.checkTextIsDateSelectedFormatV2(date2));
        System.out.println("Is " + date3 + " a date with MM.DD.YYYY format? " + StringUtils.checkTextIsDateSelectedFormat(date3));
        System.out.println("Is " + date3 + " a date with MM.DD.YYYY format? " + StringUtils.checkTextIsDateSelectedFormatV2(date3));
        System.out.println("Is " + date4 + " a date with MM.DD.YYYY format? " + StringUtils.checkTextIsDateSelectedFormat(date4));
        System.out.println("Is " + date4 + " a date with MM.DD.YYYY format? " + StringUtils.checkTextIsDateSelectedFormatV2(date4));
        System.out.println("\nSub-task 8 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(emailExample1 + " is " + StringUtils.checkEmailAddressFormat(emailExample1) + " Email Id.");
        System.out.println(emailExample2 + " is " + StringUtils.checkEmailAddressFormat(emailExample2) + " Email Id.");
        System.out.println(emailExample3 + " is " + StringUtils.checkEmailAddressFormat(emailExample3) + " Email Id.");
        System.out.println(emailExample4 + " is " + StringUtils.checkEmailAddressFormat(emailExample4) + " Email Id.");
        System.out.println(emailExample5 + " is " + StringUtils.checkEmailAddressFormat(emailExample5) + " Email Id.");
        System.out.println("\nSub-task 9 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Input phone numbers: " + Arrays.asList(arrayOfNumbers));
        System.out.println("Array of phone numbers that matches +Х(ХХХ)ХХХ-ХХ-ХХ format: " + Arrays.toString(StringUtils.getPhoneNumbers(arrayOfNumbers)));
    }
}
