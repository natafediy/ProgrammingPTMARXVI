package ua.org.oa.natafediy.hometask1;

/**
 * Created with IntelliJ IDEA.
 * User: Nata Fediy
 * Date: 4/10/2016
 * Time: 3:03 PM
 * To change this template use File | Settings | File Templates.
 */
public enum DayOfWeek {
    SUNDAY(0),
    MONDAY(1),
    TUESDAY(2),
    WEDNESDAY(3),
    THURSDAY(4),
    FRIDAY(5),
    SATURDAY(6);

    private final int dayNumber;

    DayOfWeek(int dayNumber) {
        this.dayNumber = dayNumber;
    }


    public static DayOfWeek valueOfWeekDay(int index){
        DayOfWeek week[] = DayOfWeek.values();
        for(DayOfWeek day: week ){
        if(index == day.dayNumber) return day;
        }
        return null;
    }
}
