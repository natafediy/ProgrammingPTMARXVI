package ua.org.oa.natafediy.hometask3;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Nata Fediy
 * Date: 5/4/2016
 * Time: 3:22 PM
 * To change this template use File | Settings | File Templates.
 */
class GenericStorage<T> {
    private int index = 0;
    private T[] array;

    // Создать конструктор без параметров. При вызове такого конструктора должен инициализироваться массив длиной 10.
    GenericStorage() {
        array = (T[]) new String[10];
    }

    // Создать конструктор с параметром (int size). Size - размер массива.
    GenericStorage(int size) {
        array = (T[]) new String[size];
    }

    // Метод add(T obj), который добавит новый элемент в хранилище в конец
    void add(T s) {
        if (index + 1 - array.length > 0)
            array = Arrays.copyOf(array, index + 1);
        array[index++] = s;
        System.out.println("Array with added element: " + Arrays.toString(array));
    }

    // Метод T get(int index), который возвратит элемент по индексу в масиве.
    T get(int id) {
        if (0 <= id & id < index) {
            return (T) array[id];
        }
        return null;
    }

    // Метод T[] getAll(), который вернет массив элементов. (Распечатать массив при помощи Arrays.toString(<-Ваш массив->))
    T[] getAll() {
        return (T[]) array;
    }

    // Метод update(int index, T obj), который подменит объект по заданной позиции только, если на этой позиции уже есть элемент.
    T[] update(int index, T obj) {

        if (array[index] != null) {
            array[index] = (T) obj;
        }
        return (T[]) array;
    }

    // Meтод delete(int index), который удалит элемент по индексу и захлопнет пустую ячейку в массиве, если на этой позиции уже есть элемент.
    T delete(int id) {
        T oldValue = array[id];
        int numMoved = index - id - 1;
        System.arraycopy(array, id + 1, array, id, numMoved);
        array[--index] = null;
        return (T) oldValue;

    }

    // Метод delete(T obj), который удалит заданный объект из массива
    T delete(T obj) {
        try {
            for (int i = 0; i < array.length; i++) {
                if (array[i].equals(obj)) {
                    return delete(i);
                }
            }
        } catch (NullPointerException e) {
            System.out.println("Array does not contain the required object: " + String.valueOf(obj));
        }
        return obj;
    }

    // Метод который возвратит размер массива ЗАПОЛНЕНОГО
    int getFullArraySize() {

        for (int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                return i;
            }
        }
        return array.length;
    }
}