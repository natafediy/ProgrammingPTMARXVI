package ua.org.oa.natafediy.hometask2.part1;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.*;


/**
 * Created with IntelliJ IDEA.
 * User: Nata Fediy
 * Date: 4/18/2016
 * Time: 8:40 PM
 * To change this template use File | Settings | File Templates.
 */
class StringUtils {
    private static final int MIN_LIMIT = 10;
    private static final int BEGIN_INDEX = 0;
    private static final int END_INDEX = 7;
    private static final int MAX_LIMIT = 12;
    private static final char GAP = ' ';
    private static final int DATE_PARTS = 3;
    private static char[] arrPunctuationMarks = {'"', '[', ']', '{', '}', '(', ')', ':', ';', ',', '.', '-', '!', '?'};
    private static int index = 0;
    private static String[] array;

    // функция, которая переворачивает строчку наоборот. Пример: было “Hello world!” стало “!dlrow olleH”

    static String reverseString(String phraseExample) {
        String result;
        if (phraseExample == null || phraseExample.length() <= 1)
            result = phraseExample;
        else {
            result = reverseString(phraseExample.substring(1)) + phraseExample.charAt(0);
        }

        return result;

    }

    // функция, которая определяет является ли строчка полиндромом. Пример: А роза упала на лапу Азора

    static boolean isPhrasePalindrome(String phrase) {
        String reversePhrase = reverseString(phrase);
        String newDirectPhrase = String.valueOf(getLettersFromText(phrase)).toLowerCase();
        String newReversePhrase = String.valueOf(getLettersFromText(reversePhrase)).toLowerCase();
        return newReversePhrase.equals(newDirectPhrase);
    }

    // функция которая проверяет длину строки, и если ее длина больше 10, то оставить в строке только первые 6 символов, иначе дополнить строку символами 'o' до длины 12.
    static String setNewSymbols(String phrase) {
        String newPhrase;
        if (phrase.length() > MIN_LIMIT) {
            newPhrase = phrase.substring(BEGIN_INDEX, END_INDEX);
        } else {
            StringBuilder line = new StringBuilder();
            line.append(phrase);
            while (line.length() < MAX_LIMIT) {
                line.append("o");
            }
            newPhrase = String.valueOf(line);
        }
        return newPhrase;
    }

    // функция, которая меняет местами первое и последнее слово в строчке
    static String changeFirstToLastWord(String phrase) {
        String result;
        StringBuilder firstWord = new StringBuilder();
        StringBuilder lastWord = new StringBuilder();
        StringBuilder newline1 = new StringBuilder();
        String newline = String.valueOf(getTextWithoutPunctuationMarks(getTextAsStringBuilder(phrase))).trim();
        for (int i = 0; i < newline.length(); i++) {
            if (newline.charAt(i) != GAP) {
                firstWord.append(newline.charAt(i));
            } else {
                break;
            }
        }
        for (int j = newline.length() - 1; j >= 0; j--) {
            if (newline.charAt(j) != GAP) {
                lastWord.append(newline.charAt(j));
            } else {
                break;
            }
        }

        for (int i = lastWord.length() - 1; i >= 0; i--) {
            newline1.append(lastWord.charAt(i));
        }
        if (String.valueOf(firstWord).equals(String.valueOf(newline1))) {
            result = String.valueOf(newline1);
        } else {
            result = String.valueOf(newline1.append(GAP).append(newline.substring((firstWord.length() + 1), (newline.length() - lastWord.length()))).append(firstWord));
        }
        return result;
    }

    // функция, которая меняет местами первое и последнее слово в каждом предложении. Предложения могут разделятся ТОЛЬКО знаком точки.
    static String changeFirstToLastWordEverySentence(String listOfPhrases) {
        StringBuilder newListOfPhrases = new StringBuilder();
        String[] phrase = listOfPhrases.split("[.]");
        for (String aPhrase : phrase) {
            newListOfPhrases.append(changeFirstToLastWord(aPhrase)).append(".").append(GAP);
        }
        return String.valueOf(newListOfPhrases);
    }

    // функция, которая проверяет содержит ли строка только символы 'a', 'b', 'c' или нет.
    static boolean checkLineContainsSelectedSymbols(String phrase, String regex) {
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < phrase.length(); i++) {
            if (phrase.charAt(i) != GAP) text.append(phrase.charAt(i));
        }
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(text);
        return m.matches();
    }

    // функция, которая определят является ли строчка датой формата MM.DD.YYYY
    // Version 1
    static boolean checkTextIsDateSelectedFormat(String date) {
        boolean b = false;
        String[] text = date.split("[.]");
        if (text.length != DATE_PARTS) return false;
        else {
            int month = Integer.parseInt(text[0]);
            if (12 < month || month < 1) return false;
            else {
                int day = Integer.parseInt(text[1]);
                if (31 < day || day < 1) return false;
                else {
                    int year = Integer.parseInt(text[2]);
                    if (year < 0) return false;
                    else {
                        b = isDateCorrect(month, day);
                    }
                }
            }
        }
        if (!b) System.out.println("Date " + date + " is not correct!");
        return b;
    }

    private static boolean isDateCorrect(int month, int day) {
        return (month == 2) & (day >= 1 & (day <= 28 || day <= 29)) || ((month == 4) || (month == 6) || (month == 9) || (month == 11))
                & (day >= 1 & day <= 30) || ((month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10)
                || (month == 12)) & (day >= 1 & day <= 31);
    }

    // функция, которая определят является ли строчка датой формата MM.DD.YYYY
    // Version 2
    static boolean checkTextIsDateSelectedFormatV2(String date) {
        String pattern = "^((((0[13578])|([13578])|(1[02])).(([1-9])|([0-2][0-9])|(3[01])))|(((0[469])|([469])|(11)).(([1-9])|([0-2][0-9])|(30)))|((2|02).(([1-9])|([0-2][0-9])))).\\d{4}$|^\\d{4}$";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(date);
        return m.matches();
    }

    // функция, которая определяет является ли строчка почтовым адресом
    static boolean checkEmailAddressFormat(String email) {
        Pattern p = Pattern.compile("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$");
        Matcher m = p.matcher(email);
        return m.matches();
    }

    // 	функция, которая находит все телефоны в переданном тексте формата +Х(ХХХ)ХХХ-ХХ-ХХ, и возвращает их в виде массива
    static String[] getPhoneNumbers(String[] text) {
        array = new String[0];
        Pattern p = Pattern.compile("^(\\+\\d)?\\(\\d{3}\\)\\d{3}-\\d{2}-\\d{2}$");
        for (String element : text) {
            Matcher m = p.matcher(element);
            if (m.matches()) {
                if (index + 1 - array.length > 0)
                    array = Arrays.copyOf(array, index + 1);
                array[index++] = element;
            }
        }
        return array;
    }


    private static StringBuilder getLettersFromText(String phrase) {
        StringBuilder newLine = getTextWithoutPunctuationMarks(getTextAsStringBuilder(phrase));
        for (int i = 0; i < newLine.length(); i++) {
            if (newLine.charAt(i) == GAP) {
                newLine.deleteCharAt(i);
            }
        }
        return newLine;
    }

    private static StringBuilder getTextAsStringBuilder(String phrase) {
        List<String> lines = new ArrayList<String>();
        lines.add(phrase);
        StringBuilder newText = new StringBuilder();
        for (String element : lines) {
            newText.append(element).append(" ");
        }
        return newText;
    }

    private static StringBuilder getTextWithoutPunctuationMarks(StringBuilder text) {
        for (int i = 0; i < text.length(); i++) {
            for (char arrPunctuationMark : arrPunctuationMarks) {
                if (text.charAt(i) == arrPunctuationMark) {
                    text.deleteCharAt(i);
                }
            }
        }
        return text;
    }
}
