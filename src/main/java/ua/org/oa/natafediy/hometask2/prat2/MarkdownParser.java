package ua.org.oa.natafediy.hometask2.prat2;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Nata Fediy
 * Date: 5/1/2016
 * Time: 2:19 PM
 * To change this template use File | Settings | File Templates.
 */
class MarkdownParser {
    static String convertLine(List<String> lines) {
        String h1_openTag = "<h1>";
        String h1_closeTag = "</h1>";
        String markdownHeader_h1 = "^#{1}\\w";
        String h2_openTag = "<h2>";
        String h2_closeTag = "</h2>";
        String markdownHeader_h2 = "^#{2}\\w";
        String h3_openTag = "<h3>";
        String h3_closeTag = "</h3>";
        String markdownHeader_h3 = "^#{3}\\w";
        String p_openTag = "<p>";
        String p_closeTag = "</p>";
        String emphasis_openTag = "<em>";
        String emphasis_closeTag = "</em>";
        String markdownEmphasis = "(\\*{1})(.*?)\\1";
        String strong_openTag = "<strong>";
        String strong_closeTag = "</strong>";
        String markdownStrong = "(\\*{2})(.*?)\\1";
        String link_openTag = "<a href=";
        String link_closeTag = "</a>";
        String markdownURL = "\\[[^\\[\\]]*?\\]\\(.*?\\)|^\\[*?\\]\\(.*?\\)"; // url structure is [link to site](http://site.com/)
        String regexLinkName = "\\[[^\\[\\]]*?\\]";
        String regexLink = "\\(.*?\\)";

        String[] markdownHeader = {markdownHeader_h1, markdownHeader_h2, markdownHeader_h3};
        String[][] arrayOfHeaderTags = {{h1_openTag, h1_closeTag}, {h2_openTag, h2_closeTag}, {h3_openTag, h3_closeTag}};
        String[] arrayOfEmphasisTags = {emphasis_openTag, emphasis_closeTag};
        String[] arrayOfStrongTags = {strong_openTag, strong_closeTag};
        String[] arrayOfLinkTags = {link_openTag, link_closeTag};
        String[] arrayOfSimpleTextTags = {p_openTag, p_closeTag};
        StringBuilder newLines = new StringBuilder();
        newLines.append("<html>").append("<body>");
        for (String element : lines) {
            // Header
            if (findHeader(element, markdownHeader)) {
                newLines.append(switchToHeaderTags(element, markdownHeader, arrayOfHeaderTags));
            } else {

                // Strong
                if (findRegex(element, markdownStrong)) {
                    newLines.append(switchToStrongTags(element, markdownStrong, arrayOfStrongTags, arrayOfSimpleTextTags));

                } else {
                    // Emphasis
                    if (findRegex(element, markdownEmphasis)) {
                        newLines.append(switchToEmphasisTags(element, markdownEmphasis, arrayOfEmphasisTags, arrayOfSimpleTextTags));
                    } else {
                        // Link
                        if (findRegex(element, markdownURL)) {
                            newLines.append(switchToURLTags(element, markdownURL, regexLink, regexLinkName, arrayOfLinkTags, arrayOfSimpleTextTags));
                        } else {
                            newLines.append(switchToSimpleTextTags(element, arrayOfSimpleTextTags));
                        }
                    }
                }
            }
        }
        if (findRegex(String.valueOf(newLines), markdownURL)) {
            StringBuilder lineWithURLTags = new StringBuilder();
            lineWithURLTags.append(switchToURLTags(String.valueOf(newLines), markdownURL, regexLink, regexLinkName, arrayOfLinkTags)).append("</body>").append("</html>");
            return String.valueOf(lineWithURLTags);
        } else {
            newLines.append("</body>").append("</html>");
            return String.valueOf(newLines);
        }
    }




    private static boolean findHeader(String element, String[] markdownHeader) {
        for (String aMarkdownHeader : markdownHeader) {
            if (findRegex(element, aMarkdownHeader)) {
                return true;
            }
        }
        return false;
    }

    private static boolean findRegex(String element, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(element);
        return matcher.find();
    }

    private static String lineWithoutRegex(String element, String regex) {
        try {
            Matcher matcher = getMatcher(element, regex);
            return element.substring(matcher.end() - 1);
        } catch (IllegalStateException e) {
            return null;
        }
    }

    private static String convertedRegexToString(String element, String regex) {
        try {
            Matcher matcher = getMatcher(element, regex);
            return element.substring(matcher.start(), matcher.end());
        } catch (IllegalStateException e) {
            return null;
        }
    }

    private static String convertedRegexURLToString(String element, String regex, String regexLink, String regexLinkName) {
        try {
            Matcher matcher = getMatcher(element, regex);
            String line = element.substring(matcher.start(), matcher.end());
            String linkName = convertedRegexToString(line, regexLinkName);
            String link = convertedRegexToString(line, regexLink);
            StringBuilder newLine = new StringBuilder();
            newLine.append("\"").append(link != null ? link.substring(1, (link != null ? link.length() : 0) - 1) : null).append("\">").append(linkName.substring(1, linkName.length() - 1));
            return String.valueOf(newLine);
        } catch (IllegalStateException e) {
            return null;
        }
    }

    private static Matcher getMatcher(String element, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(element);
        if (!matcher.find()) {
            throw new IllegalStateException("Regex is not found!");
        }
        return matcher;
    }

    private static int selectHeaderType(String element, String[] markdownHeader) {
        int result = 0;
        for (int i = 0; i < markdownHeader.length; i++) {
            if (findRegex(element, markdownHeader[i])) {
                result = i;
            }
        }
        return result;
    }

    private static String switchToHeaderTags(String element, String[] markdownHeader, String[][] arrayOfHeaderTags) {
        int index = selectHeaderType(element, markdownHeader);
        StringBuilder convertedLine = new StringBuilder();
        convertedLine.append(arrayOfHeaderTags[index][0]).append(lineWithoutRegex(element, markdownHeader[index])).append(arrayOfHeaderTags[index][1]);
        return String.valueOf(convertedLine);
    }

    private static StringBuilder getStringBuilderLine(String element, String markdownElement, String[] arrayOfTags, String arrayOfSimpleTextTag, int BEGIN_INDEX, int DIFFERENCE) {
        StringBuilder convertedLine = new StringBuilder();
        String[] array = element.split(markdownElement);
        String line = element;
        String regexLine = convertedRegexToString(element, markdownElement);
        convertedLine.append(arrayOfSimpleTextTag);
        for (String anArray : array) {
            if (regexLine != null) {
                convertedLine.append(anArray)
                        .append(arrayOfTags[0])
                        .append(regexLine.substring(BEGIN_INDEX, regexLine.length() - DIFFERENCE))
                        .append(arrayOfTags[1]);

                line = line.substring(anArray.length() + regexLine.length());
                regexLine = convertedRegexToString(line, markdownElement);
            } else {
                convertedLine.append(anArray);
            }
        }
        return convertedLine;
    }

    private static String switchToEmphasisTags(String element, String markdownEmphasis, String[] arrayOfEmphasisTags, String[] arrayOfSimpleTextTags) {
        final int BEGIN_INDEX = 1;
        final int DIFFERENCE = 1;
        StringBuilder convertedLine = getStringBuilderLine(element, markdownEmphasis, arrayOfEmphasisTags, arrayOfSimpleTextTags[0], BEGIN_INDEX, DIFFERENCE);
        return String.valueOf(convertedLine.append(arrayOfSimpleTextTags[1]));
    }

    private static String switchToStrongTags(String element, String markdownStrong, String[] arrayOfStrongTags, String[] arrayOfSimpleTextTags) {
        final int BEGIN_INDEX = 2;
        final int DIFFERENCE = 2;
        StringBuilder convertedLine = getStringBuilderLine(element, markdownStrong, arrayOfStrongTags, arrayOfSimpleTextTags[0], BEGIN_INDEX, DIFFERENCE);
        return String.valueOf(convertedLine.append(arrayOfSimpleTextTags[1]));
    }

    private static String switchToURLTags(String element, String markdownURL, String regexLink, String regexLinkName, String[] arrayOfLinkTags, String[] arrayOfSimpleTextTags) {
        StringBuilder convertedLine = getStringBuilderLineWithURL(element, markdownURL, convertedRegexURLToString(element, markdownURL, regexLink, regexLinkName), arrayOfLinkTags, arrayOfSimpleTextTags[0]);
        return String.valueOf(convertedLine.append(arrayOfSimpleTextTags[1]));
    }

    private static String switchToURLTags(String element, String markdownURL, String regexLink, String regexLinkName, String[] arrayOfLinkTags) {
        StringBuilder convertedLine = getStringBuilderLineWithURL(element, markdownURL, convertedRegexURLToString(element, markdownURL, regexLink, regexLinkName), arrayOfLinkTags);
        return String.valueOf(convertedLine);
    }


    private static StringBuilder getStringBuilderLineWithURL(String element, String markdownURL, String regexLine, String[] arrayOfLinkTags, String arrayOfSimpleTextTag) {
        StringBuilder convertedLine = new StringBuilder();
        String[] array = element.split(markdownURL);
        convertedLine.append(arrayOfSimpleTextTag);
        getFullLine(markdownURL, arrayOfLinkTags, convertedLine, array, element, regexLine);
        return convertedLine;
    }

    private static void getFullLine(String markdownURL, String[] arrayOfLinkTags, StringBuilder convertedLine, String[] array, String line, String regexLine) {
        for (String anArray : array) {
            if (regexLine != null) {
                convertedLine.append(anArray)
                        .append(arrayOfLinkTags[0])
                        .append(regexLine.substring(0, regexLine.length()))
                        .append(arrayOfLinkTags[1]);
                line = line.substring(anArray.length() + regexLine.length());
                regexLine = convertedRegexToString(line, markdownURL);
            } else {
                convertedLine.append(anArray);
            }
        }
    }

    private static StringBuilder getStringBuilderLineWithURL(String element, String markdownURL, String regexLine, String[] arrayOfLinkTags) {
        StringBuilder convertedLine = new StringBuilder();
        String[] array = element.split(markdownURL);
        getFullLine(markdownURL, arrayOfLinkTags, convertedLine, array, element, regexLine);
        return convertedLine;
    }

    private static String switchToSimpleTextTags(String element, String[] arrayOfSimpleTextTags) {
        StringBuilder convertedLine = new StringBuilder();
        convertedLine.append(arrayOfSimpleTextTags[0]).append(element).append(arrayOfSimpleTextTags[1]);
        return String.valueOf(convertedLine);
    }
}
