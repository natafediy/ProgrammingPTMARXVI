package ua.org.oa.natafediy.hometask1;

/**
 * Created with IntelliJ IDEA.
 * User: Nata Fediy
 * Date: 4/10/2016
 * Time: 1:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class Date {
    public static final int LEAP_YEAR_DAYS = 366;
    public static final int SIMPLE_YEAR_DAYS = 365;
    private final Day day;
    private final Month month;
    private final Year year;
    private static int dayOfWeekIndex;
    private static int dayOfYear;

    public static void main(String[] args) throws Exception {
        printDayInfo(17, 4, 2016);
        printDaysNumberBetweenTwoDates(12, 2, 2010, 17, 4, 2016);
        printDaysNumberBetweenTwoDates(12, 2, 2010, 17, 4, 2010);
        printDaysNumberBetweenTwoDates(12, 4, 2016, 17, 4, 2016);
        printDaysNumberBetweenTwoDates(12, 4, 2016, 12, 4, 2016);
    }

    private static void printDayInfo(int day, int month, int year) throws Exception {
        if ((0 < day & day < 32) & (0 < month & month < 13) & year > 0) {
            System.out.println("It was input date: " + day + "/" + month + "/" + year);
            Date date = new Date(day, month, year);
            dayOfWeekIndex = weekDayIndex(day, month, year);
            dayOfYear = calculateDayOfYear(day, month, year);
            System.out.println(date.getDayOfWeek());
            System.out.println("It is " + date.getDayOfYear() + " day in the year.");
        } else {
            System.out.println("You input incorrect date");
        }
    }

    private static void printDaysNumberBetweenTwoDates(int day1, int month1, int year1, int day2, int month2, int year2) throws Exception {
        if ((0 < day1 & day1 < 32) & (0 < month1 & month1 < 13) & year1 > 0) {
            if ((0 < day2 & day2 < 32) & (0 < month2 & month2 < 13) & year2 > 0) {

                Date date1 = new Date(day1, month1, year1);
                Date date2 = new Date(day2, month2, year2);
                System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                System.out.println("It was input two dates: \n" + day1 + "/" + month1 + "/" + year1 + " and " + day2 + "/" + month2 + "/" + year2);
                System.out.println("Between " + day1 + "/" + month1 + "/" + year1 + " and " + day2 + "/" + month2 + "/" + year2 + " " + date1.daysBetween(date2) + " days.");
                System.out.println("Between " + day2 + "/" + month2 + "/" + year2 + " and " + day1 + "/" + month1 + "/" + year1 + " " + date2.daysBetween(date1) + " days.");


            } else System.out.println("The second date is not correct");

        } else System.out.println("The first date is not correct");
    }

    public Date(int day, int month, int year) {
        this.day = new Day(day);
        this.month = new Month(month);
        this.year = new Year(year);
    }

    // Week Day discovery
    // The open source of algorithm is here: https://goo.gl/lH0BQr
    private static int weekDayIndex(int day, int month, int year) {
        int a = (14 - month) / 12;
        int y = year - a;
        int m = month + 12 * a - 2;
        return (day + y + y / 4 - y / 100 + y / 400 + (31 * m) / 12) % 7;
    }

    public DayOfWeek getDayOfWeek() {
        DayOfWeek[] dayOfWeek = DayOfWeek.values();
        return dayOfWeek[dayOfWeekIndex];
    }

    public int getDayOfYear() {
        return dayOfYear;
    }

    public static int calculateDayOfYear(int day, int month, int year) {
        int result = day;
        if (month == 1) return day;
        else {
            MonthOfYear[] months = MonthOfYear.values();
            for (int i = 0; i < month - 1; i++) {
                int index = months[i].getMonthNumber();
                switch (index) {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        result += 31;
                        break;
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        result += 30;
                        break;
                    case 2:
                        if (isLeapYear(year)) result += 29;
                        else {
                            result += 28;
                        }
                        break;
                }
            }
            return result;
        }
    }

    // Verification whether year is a leap year of not
    public static boolean isLeapYear(int year) {
        if (year % 4 == 0) return true;
        return false;
    }

    // Calculation the number of days between two dates

    public int daysBetween(Date date) throws Exception {
        int result = 0;
        int day2 = date.day.day;
        int month2 = date.month.month;
        int year2 = date.year.year;
        int day1 = this.day.day;
        int month1 = this.month.month;
        int year1 = this.year.year;

        // Common version: both dates have different values of years, months and days accordingly
        if (year1 < year2) {
            result = calculateDayOfYear(day2, month2, year2) + calculateYears(year1, year2);
            if (isLeapYear(year1)) result += (LEAP_YEAR_DAYS - calculateDayOfYear(day1, month1, year1));
            else {
                result += (SIMPLE_YEAR_DAYS - calculateDayOfYear(day1, month1, year1));
            }
        } else {
            if (year1 > year2) {
                result = calculateDayOfYear(day1, month1, year1) + calculateYears(year2, year1);
                if (isLeapYear(year2)) result += (LEAP_YEAR_DAYS - calculateDayOfYear(day2, month2, year2));
                else {
                    result += (SIMPLE_YEAR_DAYS - calculateDayOfYear(day2, month2, year2));
                }
            } else {
                // Version when two dates have the same values of year but different values of months and days
                if (month1 < month2)
                    result = calculateDayOfYear(day2, month2, year2) - calculateDayOfYear(day1, month1, year1);
                else {
                    if (month1 > month2)
                        result = calculateDayOfYear(day1, month1, year1) - calculateDayOfYear(day2, month2, year2);
                    else {
                        //Version when two dates have the same values of year and month but only different values of days
                        if (day1 < day2) result = day2 - day1;
                        else {
                            if (day1 > day2) result = day1 - day2;
                            else {
                                return result;
                            }
                        }
                    }
                }
            }
            return result;
        }
        return result;
    }

    // Calculating number of days in several years

    private int calculateYears(int startYear, int finishYear) {
        int result = 0;
        int difference = finishYear - startYear;
        if (difference != 1) {
            for (int i = startYear + 1; i < finishYear; i++) {
                if (isLeapYear(i)) result += LEAP_YEAR_DAYS;
                else {
                    result += SIMPLE_YEAR_DAYS;
                }
            }
        }
        return result;
    }

    public static class Year {
        private final int year;

        public Year(int year) {
            this.year = year;
        }
    }

    public static class Month {
        private final int month;

        public Month(int month) {
            this.month = month;
        }
    }


    public static class Day {
        private final int day;

        public Day(int day) {
            this.day = day;
        }
    }
}