package ua.org.oa.natafediy.hometask1;

/**
 * Created with IntelliJ IDEA.
 * User: Nata Fediy
 * Date: 4/10/2016
 * Time: 3:05 PM
 * To change this template use File | Settings | File Templates.
 */
public enum MonthOfYear {
    JANUARY (1),
    FEBRUARY(2),
    MARCH(3),
    APRIL(4),
    MAY(5),
    JUNE(6),
    JULY(7),
    AUGUST(8),
    SEPTEMBER(9),
    OCTOBER (10),
    NOVEMBER(11),
    DECEMBER(12);

    public int getMonthNumber() {
        return monthNumber;
    }

    private final int monthNumber;

    MonthOfYear(int monthNumber) {
        this.monthNumber = monthNumber;
    }
}
