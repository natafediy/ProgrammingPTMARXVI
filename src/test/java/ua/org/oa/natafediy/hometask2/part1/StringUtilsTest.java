package ua.org.oa.natafediy.hometask2.part1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: Nata Fediy
 * Date: 5/4/2016
 * Time: 12:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class StringUtilsTest {
    @Test
    public void reverseString_Equals() throws Exception {
        String actualPhrase = StringUtils.reverseString("Hello Friends!");
        String expectedPhrase = "!sdneirF olleH";
        Assert.assertEquals("Test for reverseString()", expectedPhrase, actualPhrase);
    }

    @Test
    public void reverseString_NotEquals() throws Exception {
        String actualPhrase = StringUtils.reverseString("Hello Friends!");
        String expectedPhrase = "!olleH";
        Assert.assertNotEquals("Test for reverseString()", expectedPhrase, actualPhrase);
    }

    @Test
    public void isPhrasePalindrome_True() throws Exception {
        String phrase = "race car";
        Assert.assertTrue("Test for isPhrasePalindrome()", StringUtils.isPhrasePalindrome(phrase));
    }

    @Test
    public void isPhrasePalindrome_False() throws Exception {
        String phrase = "Hello Friends";
        Assert.assertFalse("Test for isPhrasePalindrome()", StringUtils.isPhrasePalindrome(phrase));
    }

    @Test
    public void setNewSymbols_Equals() throws Exception {
        String actualPhrase = StringUtils.setNewSymbols("race car");
        String expectedPhrase = "race caroooo";
        Assert.assertEquals("Test for setNewSymbols()", expectedPhrase, actualPhrase);
    }

    @Test
    public void setNewSymbols_NotEquals() throws Exception {
        String actualPhrase = StringUtils.setNewSymbols("race car");
        String expectedPhrase = "race car";
        Assert.assertNotEquals("Test for setNewSymbols()", expectedPhrase, actualPhrase);
    }

    @Test
    public void changeFirstToLastWord_Equals() throws Exception {
        String actualPhrase = StringUtils.changeFirstToLastWord("Hello Friends");
        String expectedPhrase = "Friends Hello";
        Assert.assertEquals("Test for changeFirstToLastWord()", expectedPhrase, actualPhrase);
    }

    @Test
    public void changeFirstToLastWord_NotEquals() throws Exception {
        String actualPhrase = StringUtils.changeFirstToLastWord("Hello Friends");
        String expectedPhrase = "Hello Friends";
        Assert.assertNotEquals("Test for changeFirstToLastWord()", expectedPhrase, actualPhrase);
    }

    @Test
    public void changeFirstToLastWordEverySentence_Equals() throws Exception {
        String actualPhrase = StringUtils.changeFirstToLastWordEverySentence("Race. Hello Friends. Inserts the string into this character sequence.");
        String expectedPhrase = "Race. Friends Hello. sequence the string into this character Inserts. ";
        Assert.assertEquals("Test for changeFirstToLastWordEverySentece()", expectedPhrase, actualPhrase);
    }

    @Test
    public void changeFirstToLastWordEverySentence_NotEquals() throws Exception {
        String actualPhrase = StringUtils.changeFirstToLastWordEverySentence("Race. Hello Friends. Inserts the string into this character sequence.");
        String expectedPhrase = "Race. Hello Friends. Inserts the string into this character sequence.";
        Assert.assertNotEquals("Test for changeFirstToLastWordEverySentece()", expectedPhrase, actualPhrase);
    }

    @Test
    public void checkLineContainsSelectedSymbols_True() throws Exception {
        String phrase = "aa abb c ccc";
        String regex = "[abc]+";
        Assert.assertTrue("Test for checkLineContainsSelectedSymbols()", StringUtils.checkLineContainsSelectedSymbols(phrase, regex));
    }

    @Test
    public void checkLineContainsSelectedSymbols_False() throws Exception {
        String phrase = "Hello Friends!";
        String regex = "[abc]+";
        Assert.assertFalse("Test for checkLineContainsSelectedSymbols()", StringUtils.checkLineContainsSelectedSymbols(phrase, regex));
    }

    @Test
    public void checkTextIsDateSelectedFormat_True() throws Exception {
        String date = "04.23.2016";
        Assert.assertTrue("Test for checkTextIsDateSelectedFormat()", StringUtils.checkTextIsDateSelectedFormat(date));
    }

    @Test
    public void checkTextIsDateSelectedFormat_False() throws Exception {
        String date = "23.04.2016";
        Assert.assertFalse("Test for checkTextIsDateSelectedFormat()", StringUtils.checkTextIsDateSelectedFormat(date));
    }

    @Test
    public void checkTextIsDateSelectedFormat_False2() throws Exception {
        String date = "02.30.2016";
        Assert.assertFalse("Test for checkTextIsDateSelectedFormat()", StringUtils.checkTextIsDateSelectedFormat(date));
    }

    @Test
    public void checkTextIsDateSelectedFormatV2_True() throws Exception {
        String date = "04.23.2016";
        Assert.assertTrue("Test for checkTextIsDateSelectedFormatV2()", StringUtils.checkTextIsDateSelectedFormatV2(date));
    }

    @Test
    public void checkTextIsDateSelectedFormatV2_False() throws Exception {
        String date = "23.04.2016";
        Assert.assertFalse("Test for checkTextIsDateSelectedFormat()", StringUtils.checkTextIsDateSelectedFormat(date));
    }

    @Test
    public void checkTextIsDateSelectedFormatV2_False2() throws Exception {
        String date = "02.30.2016";
        Assert.assertFalse("Test for checkTextIsDateSelectedFormat()", StringUtils.checkTextIsDateSelectedFormat(date));
    }

    @Test
    public void checkEmailAddressFormat_True() throws Exception {
        String email = "very.common@example.com";
        Assert.assertTrue("Test for checkEmailAddressFormat()", StringUtils.checkEmailAddressFormat(email));

    }

    @Test
    public void checkEmailAddressFormat_False() throws Exception {
        String email = "very@common@example.com";
        Assert.assertFalse("Test for checkEmailAddressFormat()", StringUtils.checkEmailAddressFormat(email));

    }

    @Test
    public void getPhoneNumbers_Equals() throws Exception {
        String[] arrayOfPhoneNumbers = {"+9(019)191-99-08", "+1(234)567-89-01", "+3(512)824-35-50"};
        String[] actualResult = StringUtils.getPhoneNumbers(arrayOfPhoneNumbers);
        String[] expectedResult = {"+9(019)191-99-08", "+1(234)567-89-01", "+3(512)824-35-50"};
        Assert.assertArrayEquals("Test for getPhoneNumbers()", expectedResult, actualResult);
    }

    @Test
    public void getPhoneNumbers_NotEquals() throws Exception {
        String[] arrayOfPhoneNumbers = {"+(019)191-99-08", "+234-567-89-01", "(512)8243550"};
        String[] actualResult = StringUtils.getPhoneNumbers(arrayOfPhoneNumbers);
        String[] expectedResult = {"+9(019)191-99-08", "+1(234)567-89-01", "+3(512)824-35-50"};
        Assert.assertNotEquals("Test for getPhoneNumbers()", expectedResult, actualResult);
    }
}