package ua.org.oa.natafediy.hometask2.prat2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Nata Fediy
 * Date: 5/1/2016
 * Time: 2:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class MarkdownApp {
    private static final String fileName = "src/main/resources/TextForHometask2Part2.txt";
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
        String line;
        List<String> lines = new ArrayList<String>();
        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }
       System.out.println(lines);
        System.out.println(MarkdownParser.convertLine(lines));
    }
}
